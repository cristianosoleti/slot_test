import { observe } from '@nx-js/observer-util';
import { Graphics, Text, Sprite, Texture, Container, Application } from 'pixi.js';
import { Slot } from './Slot';
import { BonusGame } from './BonusGame';
import { store } from '../store';
import { SYMBOL_SIZE, SymbolRank, PlayLineNr, GAME_VIEWPORT, symbolDataMap } from '../utils/constants';
import bar from '../assets/bar.png'
import bell from '../assets/bell.png'
import seven from '../assets/seven.png'
import cherry from '../assets/cherry.png'
import spin from '../assets/spin.png'
import ticket from '../assets/ticket.png'
import gem_blue from '../assets/gem_blue.png'
import gem_green from '../assets/gem_green.png'
import gem_purple from '../assets/gem_purple.png'
import gem_red from '../assets/gem_red.png'
import gem_yellow from '../assets/gem_yellow.png'
import background from '../assets/background.png'
import gsap from 'gsap/all';

/**
 * The entry point for the app
 * Will include the slot, the bonus game and the top/bottom black bars
 */
class App extends Application {
  private readonly viewPortRatio = GAME_VIEWPORT[0] / GAME_VIEWPORT[1]
  private readonly app = new Container()
  private readonly paytableContainer = new Container()
  private slot!: Slot
  private bonusGame!: BonusGame
  private isStarted = false

  constructor () {
    super({
      sharedTicker: true,
      antialias: true,
      autoStart: false,
      transparent: true,
      autoDensity: true,
      resolution: devicePixelRatio,
      width: GAME_VIEWPORT[0],
      height: GAME_VIEWPORT[1],
    })

    this.stage.addChild(this.app)
    this.applyResize()
    window.addEventListener('resize', () => {
      this.applyResize()
    })
  }

  private applyResize () {
    const width = window.innerWidth
    const height = window.innerHeight
    let w = width
    let h = width / this.viewPortRatio

    if (width / height >= this.viewPortRatio) {
      w = height * this.viewPortRatio;
      h = height;
    }
    this.renderer.view.style.width = w + 'px';
    this.renderer.view.style.height = h + 'px';
  }

  public startApp () {
    if (this.isStarted) return
    this.isStarted = true
    document.body.appendChild(this.view);
    this.preload()
  }

  /**
   * Add textures manually
   * Avoided using spritesheet for the sake of simplicity
   */
  private preload () {
    this.loader
      .add(SymbolRank.LOW, bar)
      .add(SymbolRank.MEDIUM, bell)
      .add(SymbolRank.HIGH, cherry)
      .add(SymbolRank.EXTRA, seven)
      .add(SymbolRank.BONUS, ticket)
      .add('gem_blue', gem_blue)
      .add('gem_green', gem_green)
      .add('gem_purple', gem_purple)
      .add('gem_red', gem_red)
      .add('gem_yellow', gem_yellow)
      .add('spin', spin)
      .add('background', background)
      .load(this.init.bind(this));
  }

  private init () {
    this.slot = new Slot()
    this.app.addChild(this.slot);
    this.setupBonusGame()
    // The height of the top and bottom black bars
    const barsHeight = (this.screen.height - SYMBOL_SIZE * 3) / 2;
    this.slot.y = barsHeight - SYMBOL_SIZE;
    this.slot.x = (this.screen.width - this.slot.width) / 2
    this.setupTopBar(barsHeight)
    this.setupButtonArea(barsHeight)
    this.setupPaytableContainer()
  }

  private setupBonusGame () {
    this.bonusGame = new BonusGame()
    this.app.addChild(this.bonusGame)
    observe(() => {
      if (store.shouldShowBonus.value) {
        this.bonusGame.startBonusGame()
      }
      this.bonusGame.visible = store.shouldShowBonus.value
    })
  }

  private setupPaytableContainer () {
    this.paytableContainer.visible = false
    const background = new Graphics().beginFill(0).drawRect(0, 0, GAME_VIEWPORT[0], GAME_VIEWPORT[1])
    const paytable = new Container()
    this.paytableContainer.addChild(background, paytable)
    let previousHeight = 0
    const multiplierTextStyle = { fontSize: 28, fill: 'white' }
    for (const symbol of Object.values(symbolDataMap)) {
      let j = 0
      const symbolPayContainer = new Container()
      for (const keyValuePair of Object.entries(symbol.multiplier)) {
        const occurrencies = Number(keyValuePair[0])
        const multiplier = Number(keyValuePair[1])
        const container = new Container()
        container.y = j * 50
        j++
        for (let i = 0; i < occurrencies; i++) {
          const sprite = new Sprite(Texture.from(symbol.tag))
          sprite.width = sprite.height = 40
          sprite.x = i * sprite.width
          container.addChild(sprite)
        }
        const multiplierValue = new Text(`${(symbol.baseValue * multiplier)}`, multiplierTextStyle)
        multiplierValue.x = container.getBounds().right + 30
        multiplierValue.y = (container.height - multiplierValue.height) / 2
        container.addChild(multiplierValue)
        symbolPayContainer.addChild(container)
      }
      paytable.addChild(symbolPayContainer)
      symbolPayContainer.y = previousHeight
      previousHeight += symbolPayContainer.height + 30
    }

    const title = new Text('Paytable', { ...multiplierTextStyle, fontSize: 60 })
    title.anchor.set(0.5)
    title.position.set(this.screen.width / 2, 30)
    paytable.y = 150
    const goBackText = new Text('Go back', multiplierTextStyle)
    goBackText.interactive = true
    goBackText.buttonMode = true
    goBackText.addListener('pointerdown', () => {
      this.paytableContainer.visible = false
    })
    this.paytableContainer.addChild(title, goBackText)
    this.app.addChild(this.paytableContainer)
  }

  private setupTopBar (margin: number) {
    const top = new Graphics();
    top.beginFill(0, 1);
    top.drawRect(0, 0, this.screen.width, margin);

    const collectedScrachCards = new Container()
    const sprite = new Sprite(Texture.from(SymbolRank.BONUS))
    sprite.scale.set(0.2)
    const textStyle = { fontSize: 24, fill: 'white' }
    const label = new Text('x 0', textStyle)
    label.position.set(sprite.getBounds().right, (sprite.height - label.height) / 2)
    const instructions = new Text('', textStyle)
    instructions.position.set(label.getBounds().right + 30, label.y)
    collectedScrachCards.addChild(sprite, label, instructions)
    top.addChild(collectedScrachCards)

    observe(() => {
      label.text = `x ${store.collectedScratchCards.value}`
      instructions.text = `Collect at least ${store.ticketsThresholdForBonus} tickets to enter the bonus game`
      if (store.shouldShowBonus.value) {
        instructions.text = "Destroy the gems, and reveal their values. If you find 2 or more of the same value, you will collect them"
      }
    })

    const buttonPaytable = new Text("Paytable", { fontSize: 32, fill: 'white' })
    buttonPaytable.x = this.screen.width - buttonPaytable.width
    buttonPaytable.interactive = true
    buttonPaytable.buttonMode = true
    buttonPaytable.addListener('pointerdown', () => {
      this.paytableContainer.visible = true
    })
    top.addChild(buttonPaytable)
    this.app.addChild(top);
  }

  private setupButtonArea (margin: number) {
    const bottomBar = new Graphics();
    bottomBar.beginFill(0, 1);
    bottomBar.drawRect(0, 0, this.screen.width, margin);
    bottomBar.y = SYMBOL_SIZE * 3 + margin
    const textStyle = { fontSize: 22, fill: "white" }
    const textContainer = new Container()
    const balanceLabel = new Text('Balance:', textStyle);
    const balanceValue = new Text(store.balance.value + '', textStyle);
    const stakeLbl = new Text('Stake:', textStyle);
    const stakeValue = new Text(store.stake.value + '', textStyle);
    stakeValue.y = stakeLbl.y
    stakeValue.x = stakeLbl.getBounds().right
    balanceLabel.y = stakeLbl.getBounds().bottom
    balanceValue.x = balanceLabel.getBounds().right
    balanceValue.y = balanceLabel.y
    textContainer.addChild(balanceLabel, balanceValue, stakeLbl, stakeValue);
    textContainer.y = bottomBar.height - textContainer.height

    const roundWinningsContainer = new Container()
    const roundWinningsLabel = new Text('Winnings:', { ...textStyle, fontSize: 30 });
    const roundWinningsValue = new Text(store.roundWinnings.value + '', { ...textStyle, fontSize: 30 });
    roundWinningsValue.x = roundWinningsLabel.getBounds().right
    roundWinningsValue.y = roundWinningsLabel.y

    roundWinningsContainer.addChild(roundWinningsLabel, roundWinningsValue)
    roundWinningsContainer.position.set(this.screen.width / 2 - roundWinningsContainer.width / 2, 100)
    observe(() => {
      /**
       * Count up animation with roundings. If the value is 0 the duration is 0 (instant setting, without counting in reverse)
       */
      gsap.to(roundWinningsValue, {
        text: store.roundWinnings.value + '',
        duration: store.roundWinnings.value === 0 ? 0 : 1,
        onUpdate () {
          roundWinningsValue.text = Number(roundWinningsValue.text).toFixed(0)
        }
      })
      balanceValue.text = store.balance.value + ''
      stakeValue.text = store.stake.value + ''
    })

    const buttonSpin = new Sprite(Texture.from('spin'))
    buttonSpin.anchor.set(0.5)
    buttonSpin.scale.set(1.6)
    buttonSpin.y = bottomBar.height - buttonSpin.height / 2
    buttonSpin.x = bottomBar.width - buttonSpin.width / 2
    bottomBar.addChild(textContainer, roundWinningsContainer, buttonSpin)

    const enableButton = (shouldEnable: boolean) => {
      buttonSpin.alpha = shouldEnable ? 1 : 0.5
      buttonSpin.interactive = shouldEnable
      buttonSpin.buttonMode = shouldEnable
    }
    enableButton(true)

    this.slot.on('spin-over', () => {
      enableButton(true)
    })

    buttonSpin.addListener('pointerdown', () => {
      this.slot.spin();
      enableButton(false)
    });

    const containerLine1 = this.createLineButton(1)
    const containerLine2 = this.createLineButton(3)
    const containerLine3 = this.createLineButton(5)
    containerLine2.x = containerLine1.getBounds().right + 20
    containerLine3.x = containerLine2.getBounds().right + 20

    const buttons = new Container()
    buttons.addChild(containerLine1, containerLine2, containerLine3)
    buttons.position.set(bottomBar.width / 2 - buttons.width / 2, bottomBar.height - buttons.height - 20)
    bottomBar.addChild(buttons)

    observe(() => {
      const shouldShow = !store.shouldShowBonus.value
      buttons.visible = shouldShow
      buttonSpin.visible = shouldShow
    })

    window.addEventListener('keydown', e => {
      if (e.code === 'Enter') {
        this.slot.spin();
        enableButton(false)
      }
    })
    this.app.addChild(bottomBar);
  }


  /**
   * 
   * @param line The number of lines to activate
   * @returns 
   */
  private createLineButton (line: PlayLineNr) {
    const containerLine = new Graphics().beginFill(0).lineStyle(1, 0xffffff).drawRoundedRect(0, 0, 130, 80, 5)
    const text = new Text(`${line} Line${line > 1 ? 's' : ''}`, { fontSize: 28, fill: 'white' })
    text.anchor.set(0.5)
    text.position.set(containerLine.width / 2, containerLine.height / 2)
    containerLine.addChild(text)
    containerLine.interactive = true
    containerLine.buttonMode = true
    const lines = Array.from(Array(line + 1).keys())
    lines.shift()
    containerLine.addListener('pointerdown', () => {
      this.slot.selectLines(lines as PlayLineNr[])
      store.updateStake(line)
    })
    return containerLine
  }
}

/**
 * Export as a singleton
 */
export const appInstance = new App()