import { filters, Container } from 'pixi.js';
import { Symbol } from './Symbol';
import { SYMBOL_SIZE, getRandomSymbolData } from '../utils';
import gsap from 'gsap'

export class Reel extends Container {
  /**
   * The maximum symbols the reel can have.
   * It represents 3 symbols + 1 fake symbol, used to make the animation smooth
   */
  private readonly maxSymbols = 4
  private blurFilter = new filters.BlurFilter()

  /**
   * Symbols including the "fake" one
   */
  private symbols: Symbol[] = []

  /**
   * Symbols used for the winning computation without the fake one.
   */
  public exposedSymbols: Symbol[] = []
  public timeline!: gsap.core.Timeline

  constructor () {
    super()
    this.filters = [this.blurFilter]
    this.disableFilter()
    this.setupReelTimeLine()
    this.populateReel()
  }

  private populateReel () {
    for (let j = 0; j < this.maxSymbols; j++) {
      const symbol = new Symbol(getRandomSymbolData());
      // The first symbol is the fake one.
      if (j > 0) {
        this.exposedSymbols.push(symbol)
      }
      symbol.y = j * SYMBOL_SIZE
      symbol.width = symbol.height = SYMBOL_SIZE
      this.timeline.to(symbol, {}, 0)
      this.addChild(symbol);
      this.symbols.push(symbol)
    }
  }

  public enableFilter (): void {
    this.blurFilter.blurX = 15
    this.blurFilter.blurY = 15
  }

  public disableFilter (): void {
    this.blurFilter.blurX = 0
    this.blurFilter.blurY = 0
  }

  private setupReelTimeLine () {
    const totalOffset = SYMBOL_SIZE * this.maxSymbols
    this.timeline = gsap.timeline({
      onStart: () => {
        // Randomizes symbols at animation start, this can be easily replaceable with API driven symbols.
        this.randomizeSymbols()
        this.enableFilter()
      },
      onComplete: () => {
        this.disableFilter()
      },
      paused: true,
      defaults: {
        duration: .1,
        modifiers: {
          y: gsap.utils.unitize(y => parseFloat(y) % totalOffset)
        },
        y: `+=${totalOffset}`,
        repeat: 5,
      }
    })
  }

  public randomizeSymbols (): void {
    this.symbols.forEach(s => {
      s.updateTexture(getRandomSymbolData())
    })
  }
}
