import { observable, observe } from '@nx-js/observer-util';
import { Text, Sprite, Texture, Container } from 'pixi.js';
import { countBy, random } from 'lodash-es';
import { delay, GAME_VIEWPORT } from '../utils';
import { store } from '../store';

/**
 * The bonus game is a simple "find 3 or more of something" and collect the prizes
 */
export class BonusGame extends Container {
  private readonly textures = ['gem_blue', 'gem_green', 'gem_purple', 'gem_red', 'gem_yellow']
  /**
   * The values here will determine the "RNG". The bigger the pool, the harder it gets
   */
  private readonly availableWinningPotentials = [10, 10, 10, 10, 10, 10, 30, 30, 50, 50, 100]
  private readonly scratchContainer = new Container()
  private currentRoundCombination: number[] = []
  private amountOfDestroyedGems = observable({ value: 0 })

  constructor () {
    super()
    const background = new Sprite(Texture.from('background'))
    background.width = GAME_VIEWPORT[0]
    background.height = GAME_VIEWPORT[1]
    this.addChild(background, this.scratchContainer)
  }

  public startBonusGame () {
    for (let i = 0; i < this.textures.length; i++) {
      const container = new Container()
      const gem = new Sprite(Texture.from(this.textures[i]))
      gem.anchor.set(0, 0.5)
      const randomWinningPotential = this.availableWinningPotentials[random(1, this.availableWinningPotentials.length - 1)]
      this.currentRoundCombination.push(randomWinningPotential)
      const text = new Text(randomWinningPotential + '', { fontSize: 50, fill: 'white' })
      text.anchor.set(0.5)
      text.position.set(gem.width / 2, 0)
      text.alpha = 0
      container.addChild(gem, text)
      container.x = i * gem.width
      container.interactive = true
      container.buttonMode = true
      this.scratchContainer.addChild(container)
      container.addListener('pointerdown', () => {
        text.alpha = 1
        gem.alpha = 0
        this.amountOfDestroyedGems.value++
      })
    }
    this.scratchContainer.position.set((this.width - this.scratchContainer.width) / 2, this.height / 2)

    observe(async () => {
      if (this.amountOfDestroyedGems.value === this.textures.length) {
        await this.checkWinnings()
        this.destroyBonusGame()
      }
    })
  }

  public destroyBonusGame () {
    this.scratchContainer.removeChildren()
    this.currentRoundCombination = []
    this.amountOfDestroyedGems.value = 0
    store.stopBonus()
  }

  private async checkWinnings () {
    const groupedWinnigs = countBy(this.currentRoundCombination)
    let totalWinnings = 0
    for (const key in groupedWinnigs) {
      const occurrencies = groupedWinnigs[key]
      if (occurrencies > 1) {
        totalWinnings += Number(key) * occurrencies
      }
    }
    store.updateRoundWinnings(totalWinnings)
    // Awaiting 5seconds before returning to base game
    // Should be dependant on some animation
    await delay(2000)
  }
}
