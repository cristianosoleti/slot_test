import { observable, observe } from '@nx-js/observer-util';
import { Container, Graphics } from 'pixi.js';
import { Symbol } from './Symbol';
import { REEL_WIDTH, playLineMap, symbolDataMap, SymbolRank, SYMBOL_SIZE, PlayLineNr, MultiplierCount, delay } from '../utils';
import { Reel } from './Reel';
import { store } from '../store';
import { groupBy } from 'lodash-es';
import gsap from 'gsap'

/**
 * This component relies on the reels and their symbols being mapped in the reelsMatrix field.
 * The spinning animation won't change the symbols' position for real, giving the edge of knowing where the symbols are already.
 * Once the animation starts, the symbols are randomly shuffled and when it ends the paylines are checked.
 */
export class Slot extends Container {
  // This value can be tuned to add/remove a reel. Although you would also need to update the paylines in playLineMap as they are hard coded.
  private readonly nrOfReels = 5
  private reels: Reel[] = []
  private isSlotRunning = false
  private reelsTimeline: gsap.core.Timeline[] = []
  private reelsCompleted = observable({ value: 0 });

  /**
   * A mapping of the "visual" representation of a payline.
   * The key is the payline id.
   * The value is a graphic object which represents a line
   */
  private graphicLines: { [p in PlayLineNr]?: Graphics } = {}

  /**
   * Paylines selected for the given round.
   */
  private selectedLines: PlayLineNr[] = []

  /**
   * Data structure which holds the reels data.
   * The index represents the reel
   * The value holds the symbols reference to the given reel
   * 0: (3) [Symbol, Symbol, Symbol]
   * 1: (3) [Symbol, Symbol, Symbol]
   * 2: (3) [Symbol, Symbol, Symbol]
   * 3: (3) [Symbol, Symbol, Symbol]
   * 4: (3) [Symbol, Symbol, Symbol]
   */
  private reelsMatrix: Symbol[][] = []

  private get reelsMatrixToFlat () { return this.reelsMatrix.flat() }

  /**
   * Data structure which holds the data of the paylines. 
   * It is build selecting from the playLineMap the symbols in reelsMatrix
   * The whole slot revolves around the concept that really and truly the positions never really change
   * Hence, we can store the "Symbols" references to a dictionary where the key is the payline ID and the values are the Symbols
   * E.G 
   * 1: (5) [Symbol, Symbol, Symbol, Symbol, Symbol] 
   * 2: (5) [Symbol, Symbol, Symbol, Symbol, Symbol]
   * 3: (5) [Symbol, Symbol, Symbol, Symbol, Symbol]
   * 4: (5) [Symbol, Symbol, Symbol, Symbol, Symbol]
   * 5: (5) [Symbol, Symbol, Symbol, Symbol, Symbol]
   */
  private playLineToSymbolMap: { [p in PlayLineNr]?: Symbol[] } = {}

  constructor () {
    super()
    this.startObservers()
    this.buildReels()
    this.buildPlayLinesGfx()
    this.associatePlayLinesWithSymbols()
    // Starting the slot with the basic payline selected
    this.selectLines([1])
  }

  /**
   * Build the visual representation of the paylines and add them to the scene
   * They are shown when the selected payline wins.
   */
  private buildPlayLinesGfx () {
    const maxWidth = REEL_WIDTH * this.nrOfReels
    this.graphicLines[2] = new Graphics().beginFill(0xff0000).drawRect(0, 0, REEL_WIDTH * this.nrOfReels, 4)
    this.graphicLines[2].alpha = 0
    this.graphicLines[2].y = 220

    this.graphicLines[1] = new Graphics().beginFill(0xff0000).drawRect(0, 0, REEL_WIDTH * this.nrOfReels, 4)
    this.graphicLines[1].y = this.graphicLines[2].y + SYMBOL_SIZE
    this.graphicLines[1].alpha = 0

    this.graphicLines[3] = this.graphicLines[1].clone()
    this.graphicLines[3].y = this.graphicLines[1].y + SYMBOL_SIZE
    this.graphicLines[3].alpha = 0

    this.graphicLines[4] = new Graphics()
      .lineStyle(4, 0xff0000)
      .moveTo(0, SYMBOL_SIZE)
      .lineTo(maxWidth / 2, SYMBOL_SIZE * 4)
      .lineTo(maxWidth, SYMBOL_SIZE)
    this.graphicLines[4].alpha = 0

    this.graphicLines[5] = new Graphics()
      .lineStyle(4, 0xff0000)
      .moveTo(0, SYMBOL_SIZE * 3)
      .lineTo(maxWidth / 2, 0)
      .lineTo(maxWidth, SYMBOL_SIZE * 3)
    this.graphicLines[5].y = SYMBOL_SIZE
    this.graphicLines[5].alpha = 0

    this.addChild(...Object.values(this.graphicLines as Graphics[]))
  }

  /**
   * Iterates over the playLineMap dictionary which holds the playlines position
   * and maps them into symbols.
   * This is used to check the winnings
   * 
   * The result of this will be for example
   * 
   * 1: (5) [Symbol, Symbol, Symbol, Symbol, Symbol] 
   * 2: (5) [Symbol, Symbol, Symbol, Symbol, Symbol]
   * 3: (5) [Symbol, Symbol, Symbol, Symbol, Symbol]
   * 4: (5) [Symbol, Symbol, Symbol, Symbol, Symbol]
   * 5: (5) [Symbol, Symbol, Symbol, Symbol, Symbol]
   */
  private associatePlayLinesWithSymbols () {
    for (const entry of Object.entries(playLineMap)) {
      const key = entry[0] as unknown as PlayLineNr
      const line = entry[1]
      this.playLineToSymbolMap[key] = []
      line.forEach(tuple => {
        this.playLineToSymbolMap[key]?.push(this.reelsMatrix[tuple[0]][tuple[1]])
      })
    }
  }

  private restorePlayLines () {
    Object.values(this.graphicLines).forEach(g => {
      g!.alpha = 0
    })
  }

  private buildReels () {
    for (let i = 0; i < this.nrOfReels; i++) {
      this.reelsMatrix.push([])
      const reel = new Reel();
      reel.x = i * REEL_WIDTH;
      this.reels.push(reel);
      this.addChild(reel);
      this.reelsMatrix[i].push(...reel.exposedSymbols)
      const reelTimeline = reel.timeline
      reelTimeline.call(() => {
        this.reelsCompleted.value++
      })
      this.reelsTimeline.push(reelTimeline)
    }
  }


  public async spin () {
    if (store.shouldShowBonus.value) return
    if (this.isSlotRunning) return;
    const canPurchaseSpin = store.purchaseSpin()
    if (!canPurchaseSpin) return
    this.restorePlayLines()
    this.isSlotRunning = true;
    for (let i = 0; i < this.reelsTimeline.length; i++) {
      await delay(i * 20) // a bit of delay from one reel spinning to the next one
      this.reelsTimeline[i].restart()
    }
    return this.playLineToSymbolMap
  }

  /**
   * Computes the winning paylines and play animations correspondingly
   */
  private startObservers () {
    observe(async () => {
      if (this.reelsCompleted.value === this.nrOfReels) {
        let totalWinnings = 0
        for (const key of this.selectedLines) {
          const lineSymbols = this.playLineToSymbolMap[key]
          const info = this.getPaylineComputation(lineSymbols!)
          if (info.counter >= info.minimumCount) {
            // Filtered symbols is the array of symbols stripped of the symbols which are not winning in this payline
            const filteredSymbols = lineSymbols!.filter((l, index) => l.tag === info.storedTag && index <= info.counter)
            const timeline = gsap.timeline({ paused: true, defaults: { duration: 1, repeat: 1, yoyo: true } })
            timeline
              .to(filteredSymbols, { alpha: 0 })
              .to(this.graphicLines[key]!, { alpha: 1 }, 0)
            await timeline.play()
            // Compute winnings for the given payline
            const winnings = symbolDataMap[info.storedTag].baseValue * symbolDataMap[info.storedTag].multiplier[info.counter]!
            totalWinnings += winnings
          }
        }
        store.updateScratchCards(await this.getScratchcardTicketsCount())
        store.updateRoundWinnings(totalWinnings)
        this.reelsCompleted.value = 0
        this.isSlotRunning = false
        this.emit('spin-over')
      }
    })
  }

  /**
   * Calculates the amount of bonus tickets
   * @returns The amount of bonus tickets in the given round
   */
  private async getScratchcardTicketsCount (): Promise<number> {
    const symbolGroup = groupBy(this.reelsMatrixToFlat, 'tag')
    const bonusSymbols = symbolGroup?.[SymbolRank.BONUS] ?? []
    if (bonusSymbols.length !== 0) {
      const tween = gsap.to(bonusSymbols, {
        x: "+=20", yoyo: true, duration: .2, repeat: 5, paused: true
      })
      await tween.play()
    }
    return bonusSymbols.length
  }

  /**
   * 
   * @param arr The array of symbols in the payline to check
   * @returns last stored tag (rank) of the symbol, its counter (occurrencies) and the minumCount to have a win
   */
  private getPaylineComputation (arr: Symbol[]) {
    const storedTag = arr[0].tag
    let counter = 1 as MultiplierCount
    let minimumCount = symbolDataMap[storedTag].minimumCount
    for (let i = 0; i < arr.length; i++) {
      if (i === this.nrOfReels - 1) break
      if (storedTag === arr[i + 1].tag) {
        counter++
        continue;
      } else break
    }
    return { counter, storedTag, minimumCount }
  }

  /**
   * Selects the playlines to play
   * @param lines The array of playlines desired
   */
  public selectLines (lines: PlayLineNr[]) {
    if (this.isSlotRunning) return
    this.restorePlayLines()
    this.selectedLines = lines
    this.selectedLines.forEach(l => {
      this.graphicLines[l]!.alpha = 1
    })
  }
}