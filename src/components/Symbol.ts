import { Texture, Sprite } from 'pixi.js';
import { SymbolData, SymbolRank } from '../utils';

export class Symbol extends Sprite {
  public tag: SymbolRank

  constructor (symbolData: SymbolData) {
    super(Texture.from(symbolData.tag))
    this.tag = symbolData.tag
  }

  public updateTexture (symbolData: SymbolData): void {
    this.texture = Texture.from(symbolData.tag)
    this.tag = symbolData.tag
  }
}