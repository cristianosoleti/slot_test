import { PlayLineNr } from '../utils';
import { Slot } from '../components/Slot'

const slot = new Slot()

describe("Check slot fields", () => {
  test("Check playLineToSymbolMap has the right amount of symbols", async () => {
    const playLineToSymbolMap = await slot.spin()
    for (const key in playLineToSymbolMap) {
      expect(playLineToSymbolMap?.[key as unknown as PlayLineNr]?.length).toBe(5)
    }
  });
});