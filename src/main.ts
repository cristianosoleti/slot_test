import * as PIXI from 'pixi.js'
import { appInstance } from './components/App'
import './styles/style.scss'
import 'normalize.css'

/**
 * This portion is to hook pixi-dev tools chrome extension on your browser
 */
if (process.env.NODE_ENV === 'development') {
  window.__PIXI_INSPECTOR_GLOBAL_HOOK__?.register({ PIXI })
}

appInstance.startApp()

// Tasks from the requirements

// - Pressing Spin should return a random result. [V]
// - Set amount of winning lines. [V]
// - Provide proper documentation. [V]
// - Proper error handling should be used. []? Not sure what it meant exactly, if uses of try/catch or just guards. 

// - Responsive design. [V] - Noticed that latest version of pixi might have a bug on the renderer, 
// on actual mobile there is something off, but I really didn't have time to investigate
// - Bonus Game [V]
// - Use Docker containers.[X]
// - Utilize Docker Compose.[X]
// - Tests [V] Sample test under tests/ folder

