declare module '*.png';

declare interface Window {
  __PIXI_INSPECTOR_GLOBAL_HOOK__?: {
    register: (object: any) => void
  }
}