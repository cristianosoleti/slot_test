import { random } from 'lodash-es'
import { symbolRanks, symbolDataMap } from './constants'

export const delay = (ms: number) => new Promise(resolve => setTimeout(resolve, ms))

/**
 * Utility to get a random symbol data
 * @returns {SymbolData}
 */
export const getRandomSymbolData = () => {
  const tag = symbolRanks[random(0, symbolRanks.length - 1)]
  return symbolDataMap[tag]
}
