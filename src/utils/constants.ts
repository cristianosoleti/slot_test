export const GAME_VIEWPORT: [number, number] = [1920, 1080]
export const REEL_WIDTH = 160;
export const SYMBOL_SIZE = 150;

export type PlayLineNr = 1 | 2 | 3 | 4 | 5

/**
 * Data structure to hold the paying lines.
 * 1 - center line
 * 2 - top line
 * 3 - bottom line
 * 4 - downward arrow \/
 * 5 - upward arrow /\
 * 
 * The matrix is composed by tuples[x,y] where:
 * x-> reel index
 * y-> symbol index
 * 
 * Can enhance with other playlines, but for the sake of simplicity, it was kept simple.
 */
export const playLineMap: { [p in PlayLineNr]: [number, number][] } = {
  1: [[0, 1], [1, 1], [2, 1], [3, 1], [4, 1]],
  2: [[0, 0], [1, 0], [2, 0], [3, 0], [4, 0]],
  3: [[0, 2], [1, 2], [2, 2], [3, 2], [4, 2]],
  4: [[0, 0], [1, 1], [2, 2], [3, 1], [4, 0]],
  5: [[0, 2], [1, 1], [2, 0], [3, 1], [4, 2]],
}

export enum SymbolRank {
  LOW = "LOW",
  MEDIUM = "MEDIUM",
  HIGH = "HIGH",
  EXTRA = "EXTRA",
  BONUS = "BONUS"
}


/**
 * Utility object to store the SymbolRank as an array, for picking random values
 */
export const symbolRanks = Object.values(SymbolRank)

export type MultiplierCount = 2 | 3 | 4 | 5

/**
 * Interface for a given symbol
 * baseValue being the base multiplier
 * tag is just an arbitrary name to give to the symbol.
 * multiplier is a <KEY,VALUE> dictionary where key is the minimum number of occurrences needed to win and value, its multiplier.
 * minimumCount is a utility property which determines which is the minim count of occorrunces for the given symbol to win a prize
 * 
 * Reason for the minimumCount is that different symbols might have different multipliers scales. 
 */
export interface SymbolData {
  baseValue: number,
  tag: SymbolRank,
  multiplier: { [p in MultiplierCount]?: number }
  minimumCount: MultiplierCount
}

/**
 * Mapping for each symbol
 * As an example of different multiplier mappings, [SymbolRank.LOW] has a starting win value of minimum "2" occurrencies.
 */
export const symbolDataMap: { [p in keyof typeof SymbolRank]: SymbolData } = {
  [SymbolRank.LOW]: {
    baseValue: 5,
    tag: SymbolRank.LOW,
    multiplier: {
      2: 1, // Here
      3: 10,
      4: 15,
      5: 25
    },
    get minimumCount () {
      return Number(Object.keys(this.multiplier)[0]) as MultiplierCount
    }
  },
  [SymbolRank.MEDIUM]: {
    baseValue: 10,
    tag: SymbolRank.MEDIUM,
    multiplier: {
      3: 1,
      4: 5,
      5: 20
    },
    get minimumCount () {
      return Number(Object.keys(this.multiplier)[0]) as MultiplierCount
    }
  },
  [SymbolRank.HIGH]: {
    baseValue: 20,
    tag: SymbolRank.HIGH,
    multiplier: {
      3: 1,
      4: 10,
      5: 20
    },
    get minimumCount () {
      return Number(Object.keys(this.multiplier)[0]) as MultiplierCount
    }
  },
  [SymbolRank.EXTRA]: {
    baseValue: 50,
    tag: SymbolRank.EXTRA,
    multiplier: {
      3: 1,
      4: 10,
      5: 20
    },
    get minimumCount () {
      return Number(Object.keys(this.multiplier)[0]) as MultiplierCount
    }
  },
  [SymbolRank.BONUS]: {
    baseValue: 70,
    tag: SymbolRank.BONUS,
    multiplier: {
      3: 1,
      4: 10,
      5: 20
    },
    get minimumCount () {
      return Number(Object.keys(this.multiplier)[0]) as MultiplierCount
    }
  }
}

