import { observable } from '@nx-js/observer-util'
import { PlayLineNr } from '../utils/constants';

/**
 * Storing data in a flux pattern
 */
class Store {
  public readonly ticketsThresholdForBonus = 40
  private readonly baseStake = 1
  private _balance = observable({ value: 2000 })
  private _stake = observable({ value: this.baseStake })
  private _roundWinnings = observable({ value: 0 })
  private _collectedScratchCards = observable({ value: 0 })
  private _shouldShowBonus = observable({ value: false })

  public get shouldShowBonus () {
    return this._shouldShowBonus;
  }

  public get balance () {
    return this._balance;
  }

  public get stake () {
    return this._stake;
  }

  public get roundWinnings () {
    return this._roundWinnings;
  }

  public get collectedScratchCards () {
    return this._collectedScratchCards;
  }

  public purchaseSpin (): boolean {
    if (this._balance.value < this._stake.value) return false
    this.updateRoundWinnings(0)
    this._balance.value -= this._stake.value
    return true
  }

  public addToBalance (value: number): void {
    this._balance.value += value
  }

  public updateStake (lines: PlayLineNr): void {
    this._stake.value = this.baseStake * lines
  }

  public updateRoundWinnings (value: number): void {
    this._roundWinnings.value = value
    this.addToBalance(this._roundWinnings.value)
  }

  public resetScratchCards () {
    this._collectedScratchCards.value = 0
  }

  public updateScratchCards (value: number): void {
    this._collectedScratchCards.value += value
    this.setShouldShowBonus(this._collectedScratchCards.value >= this.ticketsThresholdForBonus)
  }

  public setShouldShowBonus (shouldShow: boolean) {
    this._shouldShowBonus.value = shouldShow
  }

  public startBonus () {
    this.setShouldShowBonus(true)
  }

  public stopBonus () {
    this.setShouldShowBonus(false)
    this.resetScratchCards()
  }
}

/**
 * Singleton
 */
export const store = new Store()