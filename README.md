# Slot test

Project developed with Pixi.js + Typescript  + Vite

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run dev
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test
```

Tests are just an example of what can be done.