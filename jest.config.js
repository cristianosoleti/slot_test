module.exports = {
  transform: {
    '^.+\\.(ts)$': 'ts-jest'
  },
  moduleFileExtensions: ['js', 'ts'],
  transformIgnorePatterns: [
    './node_modules/(?!gsap|lodash-es)'
  ],
  setupFiles: ['jest-canvas-mock'],
  globals: {
    window: {},
    PIXI: {}
  },
  moduleNameMapper: {
    '\\.(css|less|scss)$': 'jest-transform-stub',
    "^lodash-es$": "lodash"
  }
}
